<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model vova07\site\models\BannersSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="banners-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'userid') ?>

    <?= $form->field($model, 'bannerName') ?>

    <?= $form->field($model, 'b_file') ?>

    <?= $form->field($model, 'b_type') ?>

    <?php // echo $form->field($model, 'beginTime') ?>

    <?php // echo $form->field($model, 'endTime') ?>

    <?php // echo $form->field($model, 'startDay') ?>

    <?php // echo $form->field($model, 'endDay') ?>

    <?php // echo $form->field($model, 'sex') ?>

    <?php // echo $form->field($model, 'ageBegin') ?>

    <?php // echo $form->field($model, 'ageEnd') ?>

    <?php // echo $form->field($model, 'limitForPerson') ?>

    <?php // echo $form->field($model, 'country') ?>

    <?php // echo $form->field($model, 'comments') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
